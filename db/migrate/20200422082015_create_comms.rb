class CreateComms < ActiveRecord::Migration[5.1]
  def change
    create_table :comms do |t|
      t.string :username
      t.text :body
      t.references :posts1, foreign_key: true

      t.timestamps
    end
  end
end
