class CreateComments1 < ActiveRecord::Migration[5.1]
  def change
    create_table :comments1 do |t|
      t.string :username
      t.text :body
      t.references :posts1, foreign_key: true

      t.timestamps
    end
  end
end
