require 'test_helper'

class CommTest < ActiveSupport::TestCase
  test "should not save post without title" do
    com = Comm.new
    assert_not com.save, "Saved the post without a body"
  end
end
