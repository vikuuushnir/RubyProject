#require 'test_helper'

class Posts1Test < ActiveSupport::TestCase

  test "should not save post without title" do
    post = Posts1.new
    assert_not post.save, "Saved the post without a title"
  end
end
