class CommsController < ApplicationController

def create
@posts1 = Posts1.find(params[:posts1_id])
@comm = @posts1.comms.create(comm_params)
redirect_to posts1_path(@posts1)
end

private def comm_params
  params.require(:comm).permit(:username, :body)
end

end
