class Posts1 < ApplicationRecord
  belongs_to :user
  has_many :comms, dependent: :destroy
	validates :title, presence: true, length: {minimum: 5}
end
