Rails.application.routes.draw do

root 'posts1s#index', as: 'home'
#root 'posts#index', as: 'home'
#get 'pages#home'
get 'about' => 'pages#about', as: 'about'

resources :posts1s do
	resources :comms
end
devise_for :users
resources :posts do
	resources :comments
end

end
